<h3>What is Docker?</h3>
Docker is a container platform. Docker is a tool designed to make it easier to deploy and run applications by using containers. We can say it is a Contanerization technique.
Docker is a set of platform as a service products that uses OS level virtualization to deliver software in packages called containers.  The software which hosts the container is called docker engine.

<br>![Docker](/extras/Docker.png)</br>

<h3>Containers and virtual Machines</h3>
Containers allow a developer to package an application with all of the part it needs. Such as libraries or other dependencies and then ship all in one package. Containers are isolated from one another and bundle their own software, libraries and configuration files, they can communicate with each other through well defined channels.

<br>![Dockercontainers](/extras/Docker Containers.png) </br>

<h3>How Docker Works</h3>
In a docker flow, a developer define all dependencies and requirements in a file called docker file.This file is used to create docker images. So, in Docker image you have all requirements and dependencies contained in it. When you run docker image you get docker containers. Docker containers are run time instances. These image can also be stored in online cloud storage repository called [Docker Hub](https://hub.docker.com). There are lot public storage images available at docker hub, you can also stored your image as well. These images can be used to create run environment such as staging environment or test environment. 

<br>![Docker workflow](/extras/Docker workflow.png)</br> 

<h3>Docker images and containers<h3>

A Docker containers is a running process which isolated the host from other unused containers. 

An Docker image includes everything needed to run an application - the code or binary, runtimes, dependencies, and any other filesystem objects required.

<br>![Docker images and containers](/extras/Docker images and containers.jpg)</br>


<h3>Difference between virtualization and containerization</h3>
In **virtualization** we have a software called hypervisor and by using this we can  create multiple virtual machines on same OS or same host. Also, in virtualization we have to allocate fixed resources.
In **containerization** we have a container engine and we donot need separate OS. We have containers where we have all lib or dependencies and it will use the whole operating system. The storage and memories are not fixed.  Containers are light weight as compared to VM. 

<br>![virtualization and contanerization](/extras/virtualization and contanerization.png)</br>

<h3> Docker Architecture</h3>
<br>Docker has a client-server Architecture. Command line is the docker interface. We have a docker server or docker daemon which will have all the containers. Docker server receives command in the form of commands or rest API request from the Docker Client. 
Docker client and Docker platform can be present on same host or different host.</br>

<br>![Docker Architecture](/extras/Docker architecture.png)</br>

<h3>Benefits of Docker</h3>
An application inside a conatainer can run on any system that has docker installed. So there is no need to configure apps multiple times on different platform.
With Docker you can test your application inside a container and ship it inside a container. It means the environment in which you test is identical to the one in which app will run in production.
Also, Portability is a good feature in docker container. Suppose we create software for amazon it will also run in VM Machine.
Docker containers work like git repositories, allowing you to commit docker changes and version control them.

<strong>Docker Commands Summary</strong>
<br>![Docker commands](/extras/docker-commands.png)</br>
 















