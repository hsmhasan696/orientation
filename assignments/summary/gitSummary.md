<h3>What is Git?</h3>
Git is a Distributed version control system(DVCS) for tracking changes in computer files.
<br>Coordinates work between multiple developers</br>
<br>Who made what changes and when</br> 
<br>Revert back at any time</br>
<br>Local and remote repositories</br>
<br>Keeps track of code history</br>
<br>You can stage files before committing</br>
![gitworking](/extras/gitworking.png)

<h3>How Git works</h3>
<br> After installing git, Run command git init  and a hidden folder created named as .git and this directory converts into local repository.</br>
<br>After the above process, 3 stages created.</br>
<strong><br>1. Workspace/working Directory -       </strong> This is the area of workspace for programmer where he/she can create,modify and edit code.</br>
<strong><br>2. Staging Area - </strong> After working directory, we add useful code to other stage which are finalized called staging area and the process called add process.</br>
<strong><br>3. Local Repository - </strong> Now, we have to commit the code. Commit means we are taking snapshot of code or saving the code. And then it moves to local repository. After code moves into local repository we have to push the code to remote repository called gitlab.   
![howgitworks](/extras/howgitworks.png)


<h3>Commands of git</h3>

<strong><br>$git init</strong> - initialize local git repository</br> 
<strong><br>$git add<file></strong> - Add files to index</br>
<strong><br>$git status</strong> - Check status of working tree</br>
<strong><br>$git commit</strong> - commit changes in index</br>
<strong><br>$git push</strong> - Push to remote repository</br>
<strong><br>$git pull</strong> - Pull from latest repository</br>
<strong><br>$git clone</strong> - clone repository into a New Directory</br>
<strong><br>$sudo apt-get install git</strong>- installation in ubuntu</br>
<strong><br>$git config --global user.name</strong></br>

<h3>Stages of git/workflow</h3>
<br> Git Workflow </br>
<br><strong>Branch</strong></br> - It is a version of repository that diverges from the main working project.
<br><strong>Clone</strong></br> - It takes the entire copy of the online repository and makes it in local repository.
<br><strong>Fetch</strong></br> - It is used to fetch branches and tags from one or more repositories, along with object which are necessary to complete the previous part.
<br><strong>Head</strong></br> - Head is the representation of last commit in current checkout branch. 

![gitworkflow](/extras/gitworkflow.png)


<strong><br>Modified:</strong></br>

So, under Git we can create a repository, a repository is basically a kind of a directory that contains all the files related to your code. So, in the repository we can write code, and maintain it. Once, the code is written, anyone willing to make some modification can make those changes in their own remote repository. A remote repository is a local copy (one that you create on your local machine) of the original project that is being maintained via Git. So, basically you can make changes to your copy of the project without Disturbing the original code. This is called Modification, i.e. making some additions to the original project.

<strong><br>Staged:</strong></br>

So, we saw that we can make changes to the project without disturbing the original version, but how do we apply those changes to our remote repository? So, we use the commands in the Git command line — git add. So, this command tracks the new changes and pushes it to the staging area. So, staging area is place prior to the actual implementation of changes, i.e. this area contains all the added files that contain new code, which are ready to be joined to the remote repository. So, all the new files are first pushed to the staging area. This can also be understood with an analogy. We all must have participated in a race or some sort of athletic event. Before, the race begins, we hear three words, Get, Set…, Go! Now, we can think of ‘Set’ as the staging area. So, this is an indication that make yourself ready, as the race is about to start. Similarly, staging area is a place where all the new files are finally ready to be joined to the remote repository.

<strong><br>Commit:</strong></br>
This is the final stage, as this stage finally applies the new changes to the remote repository. Looking at the previous analogy, this is your ‘Go’ position. So, a commit is a set of new files that are being added to a project as part of the modification. Each commit represents the changes made to project in the past, with the details about the time at which commit was made and the author of the code. So, finally when you make a commit, and it gets committed, then this simply means that you have successfully applied a certain modification to the code.
![stagesofigt](/extras/Stages of git.png)
